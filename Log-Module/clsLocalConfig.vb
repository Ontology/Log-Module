﻿Imports Ontology_Module
Imports OntologyClasses.BaseClasses
Imports System.Reflection
Imports System.Runtime.InteropServices
Imports ImportExport_Module
Imports OntologyAppDBConnector
Imports OntologyClasses.Interfaces

Public Class clsLocalConfig
    Implements ILocalConfig
    Private objImport As ImportWorker

    Private cstrID_Ontology As String = "eaa3b647c2224c79abb93c6f5d9306ab"

    Private objGlobals As Globals

    Private objOItem_DevConfig As New clsOntologyItem

    Private objDBLevel_Config1 As OntologyModDBConnector
    Private objDBLevel_Config2 As OntologyModDBConnector

    Private objOItem_Attribute_DateTimeStamp As New clsOntologyItem
    Private objOItem_attribute_dbPostfix As New clsOntologyItem
    Private objOItem_Attribute_Message As New clsOntologyItem
    Private objOItem_RelationType_belongsTo As New clsOntologyItem
    Private objOItem_RelationType_provides As New clsOntologyItem
    Private objOItem_RelationType_wasCreatedBy As New clsOntologyItem
    Private objOItem_Type_LogEntry As New clsOntologyItem
    Private objOItem_type_Logstate As New clsOntologyItem
    Private objOItem_type_User As New clsOntologyItem

    Private objOItem_User As clsOntologyItem

    Public ReadOnly Property Id_Ontology As String
        Get
            Return cstrID_Ontology
        End Get
    End Property

    Public ReadOnly Property OItem_Attribute_DateTimeStamp() As clsOntologyItem
        Get
            Return objOItem_Attribute_DateTimeStamp
        End Get
    End Property

    Public ReadOnly Property OItem_attribute_dbPostfix() As clsOntologyItem
        Get
            Return objOItem_attribute_dbPostfix
        End Get
    End Property

    Public ReadOnly Property OItem_Attribute_Message() As clsOntologyItem
        Get
            Return objOItem_Attribute_Message
        End Get
    End Property

    Public ReadOnly Property OItem_RelationType_belongsTo() As clsOntologyItem
        Get
            Return objOItem_RelationType_belongsTo
        End Get
    End Property

    Public ReadOnly Property OItem_RelationType_provides() As clsOntologyItem
        Get
            Return objOItem_RelationType_provides
        End Get
    End Property

    Public ReadOnly Property OItem_RelationType_wasCreatedBy() As clsOntologyItem
        Get
            Return objOItem_RelationType_wasCreatedBy
        End Get
    End Property

    Public ReadOnly Property OItem_Type_LogEntry() As clsOntologyItem
        Get
            Return objOItem_Type_LogEntry
        End Get
    End Property

    Public ReadOnly Property OItem_type_Logstate() As clsOntologyItem
        Get
            Return objOItem_type_Logstate
        End Get
    End Property

    Public ReadOnly Property OItem_type_User() As clsOntologyItem
        Get
            Return objOItem_type_User
        End Get
    End Property


    Public Property OItem_User As clsOntologyItem
        Get
            Return objOItem_User
        End Get
        Set(ByVal value As clsOntologyItem)
            objOItem_User = value
        End Set
    End Property

    Private Sub get_Data_DevelopmentConfig()
        Dim objORL_Ontology_To_OntolgyItems = New List(Of clsObjectRel) From {New clsObjectRel With {.ID_Object = cstrID_Ontology, _
                                                                                             .ID_RelationType = objGlobals.RelationType_contains.GUID, _
                                                                                             .ID_Parent_Other = objGlobals.Class_OntologyItems.GUID}}



        Dim objOItem_Result = objDBLevel_Config1.GetDataObjectRel(objORL_Ontology_To_OntolgyItems, doIds:=False)
        If objOItem_Result.GUID = objGlobals.LState_Success.GUID Then
            If objDBLevel_Config1.ObjectRels.Any Then

                objORL_Ontology_To_OntolgyItems = objDBLevel_Config1.ObjectRels.Select(Function(oi) New clsObjectRel With {.ID_Object = oi.ID_Other,
                                                                                                                                .ID_RelationType = objGlobals.RelationType_belongingAttribute.GUID}).ToList()
                objORL_Ontology_To_OntolgyItems.AddRange(objDBLevel_Config1.ObjectRels.Select(Function(oi) New clsObjectRel With {.ID_Object = oi.ID_Other,
                                                                                                                                .ID_RelationType = objGlobals.RelationType_belongingClass.GUID}))
                objORL_Ontology_To_OntolgyItems.AddRange(objDBLevel_Config1.ObjectRels.Select(Function(oi) New clsObjectRel With {.ID_Object = oi.ID_Other,
                                                                                                                                .ID_RelationType = objGlobals.RelationType_belongingObject.GUID}))
                objORL_Ontology_To_OntolgyItems.AddRange(objDBLevel_Config1.ObjectRels.Select(Function(oi) New clsObjectRel With {.ID_Object = oi.ID_Other,
                                                                                                                                .ID_RelationType = objGlobals.RelationType_belongingRelationType.GUID}))

                objOItem_Result = objDBLevel_Config2.GetDataObjectRel(objORL_Ontology_To_OntolgyItems, doIds:=False)
                If objOItem_Result.GUID = objGlobals.LState_Success.GUID Then
                    If Not objDBLevel_Config2.ObjectRels.Any Then
                        Err.Raise(1, "Config-Error")
                    End If
                Else
                    Err.Raise(1, "Config-Error")
                End If

            Else
                Err.Raise(1, "Config-Error")
            End If

        End If

    End Sub

    Public ReadOnly Property Globals() As Globals
        Get
            Return objGlobals
        End Get
    End Property

    Public Sub New(ByVal Globals As Globals)
        objGlobals = Globals
        set_DBConnection()


        get_Config()
    End Sub

    Private Sub set_DBConnection()
        objDBLevel_Config1 = New OntologyModDBConnector(objGlobals)
        objDBLevel_Config2 = New OntologyModDBConnector(objGlobals)
        objImport = New ImportWorker(objGlobals)
    End Sub

    Private Sub get_Config()
        Try
            get_Data_DevelopmentConfig()
            get_Config_AttributeTypes()
            get_Config_RelationTypes()
            get_Config_Classes()
            get_Config_Objects()
        Catch ex As Exception
            Dim objAssembly = [Assembly].GetExecutingAssembly()
            Dim objCustomAttributes() As AssemblyTitleAttribute = objAssembly.GetCustomAttributes(GetType(AssemblyTitleAttribute), False)
            Dim strTitle = "Unbekannt"
            If objCustomAttributes.Length = 1 Then
                strTitle = objCustomAttributes.First().Title
            End If
            If MsgBox(strTitle & ": Die notwendigen Basisdaten konnten nicht geladen werden! Soll versucht werden, sie in der Datenbank " & _
                      objGlobals.Index & "@" & objGlobals.Server & " zu erzeugen?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                Dim objOItem_Result = objImport.ImportTemplates(objAssembly)
                If Not objOItem_Result.GUID = objGlobals.LState_Error.GUID Then
                    get_Data_DevelopmentConfig()
                    get_Config_AttributeTypes()
                    get_Config_RelationTypes()
                    get_Config_Classes()
                    get_Config_Objects()
                Else
                    Err.Raise(1, "Config not importable")
                End If
            Else
                Environment.Exit(0)
            End If
        End Try


    End Sub

    Private Sub get_Config_AttributeTypes()
        Dim objOList_attribute_datetimestamp = (From objOItem In objDBLevel_Config1.ObjectRels
                                           Where objOItem.ID_Object = cstrID_Ontology
                                           Join objRef In objDBLevel_Config2.ObjectRels On objOItem.ID_Other Equals objRef.ID_Object
                                           Where objRef.Name_Object.ToLower() = "attribute_datetimestamp".ToLower() And objRef.Ontology = objGlobals.Type_AttributeType
                                           Select objRef).ToList()

        If objOList_attribute_datetimestamp.Any() Then
            objOItem_Attribute_DateTimeStamp = New clsOntologyItem
            objOItem_Attribute_DateTimeStamp.GUID = objOList_attribute_datetimestamp.First().ID_Other
            objOItem_Attribute_DateTimeStamp.Name = objOList_attribute_datetimestamp.First().Name_Other
            objOItem_Attribute_DateTimeStamp.GUID_Parent = objOList_attribute_datetimestamp.First().ID_Parent_Other
            objOItem_Attribute_DateTimeStamp.Type = objGlobals.Type_AttributeType
        Else
            Err.Raise(1, "config err")
        End If

        Dim objOList_attribute_dbpostfix = (From objOItem In objDBLevel_Config1.ObjectRels
                                           Where objOItem.ID_Object = cstrID_Ontology
                                           Join objRef In objDBLevel_Config2.ObjectRels On objOItem.ID_Other Equals objRef.ID_Object
                                           Where objRef.Name_Object.ToLower() = "attribute_dbpostfix".ToLower() And objRef.Ontology = objGlobals.Type_AttributeType
                                           Select objRef).ToList()

        If objOList_attribute_dbpostfix.Any() Then
            objOItem_attribute_dbPostfix = New clsOntologyItem
            objOItem_attribute_dbPostfix.GUID = objOList_attribute_dbpostfix.First().ID_Other
            objOItem_attribute_dbPostfix.Name = objOList_attribute_dbpostfix.First().Name_Other
            objOItem_attribute_dbPostfix.GUID_Parent = objOList_attribute_dbpostfix.First().ID_Parent_Other
            objOItem_attribute_dbPostfix.Type = objGlobals.Type_AttributeType
        Else
            Err.Raise(1, "config err")
        End If

        Dim objOList_attribute_message = (From objOItem In objDBLevel_Config1.ObjectRels
                                           Where objOItem.ID_Object = cstrID_Ontology
                                           Join objRef In objDBLevel_Config2.ObjectRels On objOItem.ID_Other Equals objRef.ID_Object
                                           Where objRef.Name_Object.ToLower() = "attribute_message".ToLower() And objRef.Ontology = objGlobals.Type_AttributeType
                                           Select objRef).ToList()

        If objOList_attribute_message.Any() Then
            objOItem_Attribute_Message = New clsOntologyItem
            objOItem_Attribute_Message.GUID = objOList_attribute_message.First().ID_Other
            objOItem_Attribute_Message.Name = objOList_attribute_message.First().Name_Other
            objOItem_Attribute_Message.GUID_Parent = objOList_attribute_message.First().ID_Parent_Other
            objOItem_Attribute_Message.Type = objGlobals.Type_AttributeType
        Else
            Err.Raise(1, "config err")
        End If


    End Sub

    Private Sub get_Config_RelationTypes()
        Dim objOList_relationtype_belongsto = (From objOItem In objDBLevel_Config1.ObjectRels
                                           Where objOItem.ID_Object = cstrID_Ontology
                                           Join objRef In objDBLevel_Config2.ObjectRels On objOItem.ID_Other Equals objRef.ID_Object
                                           Where objRef.Name_Object.ToLower() = "relationtype_belongsto".ToLower() And objRef.Ontology = objGlobals.Type_RelationType
                                           Select objRef).ToList()

        If objOList_relationtype_belongsto.Any() Then
            objOItem_RelationType_belongsTo = New clsOntologyItem
            objOItem_RelationType_belongsTo.GUID = objOList_relationtype_belongsto.First().ID_Other
            objOItem_RelationType_belongsTo.Name = objOList_relationtype_belongsto.First().Name_Other
            objOItem_RelationType_belongsTo.Type = objGlobals.Type_RelationType
        Else
            Err.Raise(1, "config err")
        End If

        Dim objOList_relationtype_provides = (From objOItem In objDBLevel_Config1.ObjectRels
                                           Where objOItem.ID_Object = cstrID_Ontology
                                           Join objRef In objDBLevel_Config2.ObjectRels On objOItem.ID_Other Equals objRef.ID_Object
                                           Where objRef.Name_Object.ToLower() = "relationtype_provides".ToLower() And objRef.Ontology = objGlobals.Type_RelationType
                                           Select objRef).ToList()

        If objOList_relationtype_provides.Any() Then
            objOItem_RelationType_provides = New clsOntologyItem
            objOItem_RelationType_provides.GUID = objOList_relationtype_provides.First().ID_Other
            objOItem_RelationType_provides.Name = objOList_relationtype_provides.First().Name_Other
            objOItem_RelationType_provides.Type = objGlobals.Type_RelationType
        Else
            Err.Raise(1, "config err")
        End If

        Dim objOList_relationtype_wascreatedby = (From objOItem In objDBLevel_Config1.ObjectRels
                                           Where objOItem.ID_Object = cstrID_Ontology
                                           Join objRef In objDBLevel_Config2.ObjectRels On objOItem.ID_Other Equals objRef.ID_Object
                                           Where objRef.Name_Object.ToLower() = "relationtype_wascreatedby".ToLower() And objRef.Ontology = objGlobals.Type_RelationType
                                           Select objRef).ToList()

        If objOList_relationtype_wascreatedby.Any() Then
            objOItem_RelationType_wasCreatedBy = New clsOntologyItem
            objOItem_RelationType_wasCreatedBy.GUID = objOList_relationtype_wascreatedby.First().ID_Other
            objOItem_RelationType_wasCreatedBy.Name = objOList_relationtype_wascreatedby.First().Name_Other
            objOItem_RelationType_wasCreatedBy.Type = objGlobals.Type_RelationType
        Else
            Err.Raise(1, "config err")
        End If


    End Sub

    Private Sub get_Config_Objects()

    End Sub

    Private Sub get_Config_Classes()
        Dim objOList_type_logentry = (From objOItem In objDBLevel_Config1.ObjectRels
                                           Where objOItem.ID_Object = cstrID_Ontology
                                           Join objRef In objDBLevel_Config2.ObjectRels On objOItem.ID_Other Equals objRef.ID_Object
                                           Where objRef.Name_Object.ToLower() = "type_logentry".ToLower() And objRef.Ontology = objGlobals.Type_Class
                                           Select objRef).ToList()

        If objOList_type_logentry.Any() Then
            objOItem_Type_LogEntry = New clsOntologyItem
            objOItem_Type_LogEntry.GUID = objOList_type_logentry.First().ID_Other
            objOItem_Type_LogEntry.Name = objOList_type_logentry.First().Name_Other
            objOItem_Type_LogEntry.GUID_Parent = objOList_type_logentry.First().ID_Parent_Other
            objOItem_Type_LogEntry.Type = objGlobals.Type_Class
        Else
            Err.Raise(1, "config err")
        End If

        Dim objOList_type_logstate = (From objOItem In objDBLevel_Config1.ObjectRels
                                           Where objOItem.ID_Object = cstrID_Ontology
                                           Join objRef In objDBLevel_Config2.ObjectRels On objOItem.ID_Other Equals objRef.ID_Object
                                           Where objRef.Name_Object.ToLower() = "type_logstate".ToLower() And objRef.Ontology = objGlobals.Type_Class
                                           Select objRef).ToList()

        If objOList_type_logstate.Any() Then
            objOItem_type_Logstate = New clsOntologyItem
            objOItem_type_Logstate.GUID = objOList_type_logstate.First().ID_Other
            objOItem_type_Logstate.Name = objOList_type_logstate.First().Name_Other
            objOItem_type_Logstate.GUID_Parent = objOList_type_logstate.First().ID_Parent_Other
            objOItem_type_Logstate.Type = objGlobals.Type_Class
        Else
            Err.Raise(1, "config err")
        End If

        Dim objOList_type_user = (From objOItem In objDBLevel_Config1.ObjectRels
                                           Where objOItem.ID_Object = cstrID_Ontology
                                           Join objRef In objDBLevel_Config2.ObjectRels On objOItem.ID_Other Equals objRef.ID_Object
                                           Where objRef.Name_Object.ToLower() = "type_user".ToLower() And objRef.Ontology = objGlobals.Type_Class
                                           Select objRef).ToList()

        If objOList_type_user.Any() Then
            objOItem_type_User = New clsOntologyItem
            objOItem_type_User.GUID = objOList_type_user.First().ID_Other
            objOItem_type_User.Name = objOList_type_user.First().Name_Other
            objOItem_type_User.GUID_Parent = objOList_type_user.First().ID_Parent_Other
            objOItem_type_User.Type = objGlobals.Type_Class
        Else
            Err.Raise(1, "config err")
        End If


    End Sub

    Public Sub New()

    End Sub

    Public ReadOnly Property IdLocalConfig() As String Implements ILocalConfig.IdLocalConfig
        Get
            Dim objAttrib = Assembly.GetExecutingAssembly().GetCustomAttributes(True).FirstOrDefault(Function(objAttribute) TypeOf (objAttribute) Is GuidAttribute)
            If Not objAttrib Is Nothing Then
                Return DirectCast(objAttrib, GuidAttribute).Value
            Else
                Return Nothing
            End If
        End Get
    End Property

End Class
